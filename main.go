package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	ui "github.com/gizak/termui/v3"
)

const (
	version = "1.0.0"
)

type styleType int

const (
	bold styleType = 1 << (iota + 9)
	underline
	reverse
)

// Application
var (
	swiper         *swiperWidget
	pathHistory    history
	updateInterval = time.Second
)

type location struct {
	path  string
	time  time.Time
	count uint
}

type history []location

func main() {
	var verbose, addPath, writeConfig bool
	var configPath string
	flag.BoolVar(&verbose, "v", false, "verbose output")
	flag.BoolVar(&addPath, "a", false, "add current path to list.")
	flag.BoolVar(&writeConfig, "write-config", false, "add current path to list.")
	flag.StringVar(&configPath, "config-path", "", "path to config")
	flag.Parse()

	if !verbose {
		log.SetOutput(io.Discard)
	}

	if writeConfig {
		var path string
		if configPath != "" {
			path = configPath
		} else {
			path = configFiles[1]
		}
		if err := writeDefaultSettings(path); err != nil {
			log.Fatalf("Failed to write config to %s: %v", path, err)
		}
		log.Printf("Wrote Default config to `%s`", path)
		os.Exit(0)
	}

	if addPath {
		addCurrentPath()
		os.Exit(0)
	}

	config, err := loadConfig()
	if err != nil {
		log.Fatalf("failed to load config: %v", err)
	}
	app := initApp(config)
	ui.Render(app.grid)
	if eventLoop(app) {
		ui.Close()
		swiper.pathHistory[swiper.SelectedRow].gotoPath()
	} else {
		ui.Close()
	}
}

// eventLoop endless loop that controls actions in widget.
func eventLoop(app App) bool {
	drawTicker := time.NewTicker(updateInterval).C

	// handles kill signal sent to gotop
	sigTerm := make(chan os.Signal, 2)
	signal.Notify(sigTerm, os.Interrupt, syscall.SIGTERM)

	uiEvents := ui.PollEvents()

	for {
		select {
		case <-sigTerm:
			return false
		case <-drawTicker:
		case e := <-uiEvents:
			switch e.ID {
			case "q", "<C-c>", "<>":
				return false
			case "<Resize>":
				payload := e.Payload.(ui.Resize)
				// termWidth, termHeight := payload.Width, payload.Height
				app.grid.SetRect(0, 0, payload.Width, payload.Height)
				ui.Clear()
			}
			switch e.ID {
			case "<Resize>":
				ui.Render(app.grid)
			case "<Enter>":
				return true
			case "<MouseLeft>": // mouse click
				payload := e.Payload.(ui.Mouse)
				swiper.HandleClick(payload.X, payload.Y)
				ui.Render(swiper)
			default:
				if action, ok := (*app.CurrentMode)[e.ID]; ok {
					action(swiper.Table)
					ui.Render(swiper)
				}
			}
		}
	}
}

// addCurrentPath ...
func addCurrentPath() {
	p := currentPath()
	now := time.Now().Unix()
	file := os.ExpandEnv(historyFile)
	f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	fmt.Fprintf(w, "%s %d\n", p, now)
	defer w.Flush()
}

func currentPath() string {
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	return path
}

func (l location) gotoPath() {
	fmt.Println(l.path)
}

func (s *swiperWidget) setColors(scheme Colorscheme) {
	s.Style = ui.NewStyle(ui.Color(scheme.Fg), ui.Color(scheme.Bg))
	s.CursorStyle = ui.NewStyle(ui.Color(scheme.CursorFg), ui.Color(scheme.CursorBg))
	s.Table.Block.TitleStyle = ui.NewStyle(ui.Color(scheme.BorderLabel), ui.Color(scheme.Bg))
	s.Table.Block.BorderStyle = ui.NewStyle(ui.Color(scheme.BorderLine), ui.Color(scheme.Bg))

}

type Mode map[string]func(T *Table)

type App struct {
	grid *ui.Grid

	CurrentMode  *Mode
	HistoryMode  *Mode
	FavoriteMode *Mode
	TreeMode     *Mode
}

func initApp(config Settings) App {
	if err := ui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v", err)
	}
	newApp := App{
		HistoryMode: NewHistoryMode(config.Keys),
	}
	newApp.CurrentMode = newApp.HistoryMode
	// init Widget
	swiper = newSwiperWidget()
	swiper.setColors(config.Colorscheme)
	// init grid
	newApp.grid = ui.NewGrid()
	newApp.grid.Set(
		ui.NewRow(.5/2, nil),
		ui.NewRow(1.0/2, swiper),
	)
	termWidth, termHeight := ui.TerminalDimensions()
	newApp.grid.SetRect(0, 0, termWidth, termHeight)

	return newApp
}
