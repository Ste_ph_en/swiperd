package main

import (
	"fmt"
	"image"
	"log"
	"strings"

	ui "github.com/gizak/termui/v3"
)

// Table ...
type Table struct {
	*ui.Block

	Header []string
	Rows   [][]string

	ColWidths []int
	ColGap    int
	PadLeft   int

	ShowCursor  bool
	CursorStyle ui.Style
	Style       ui.Style

	ShowLocation bool

	UniqueCol    int    // the column used to uniquely identify each table row
	SelectedItem string // used to keep the cursor on the correct item if the data changes
	SelectedRow  int
	TopRow       int // used to indicate where in the table we are scrolled at

	ColResizer func()
}

// NewTable returns a new Table instance
func NewTable() *Table {
	return &Table{
		Block:       ui.NewBlock(),
		SelectedRow: 0,
		TopRow:      0,
		UniqueCol:   0,
		ColResizer:  func() {},
	}
}

// Draw ...
func (T *Table) Draw(buf *ui.Buffer) {
	T.Block.Draw(buf)

	if T.ShowLocation {
		T.drawLocation(buf)
	}

	T.ColResizer()

	// finds exact column starting position
	colXPos := []int{}
	cur := 1 + T.PadLeft
	for _, w := range T.ColWidths {
		colXPos = append(colXPos, cur)
		cur += w
		cur += T.ColGap
	}

	// prints header
	for i, h := range T.Header {
		width := T.ColWidths[i]
		if width == 0 {
			continue
		}
		// don't render column if it doesn't fit in widget
		if width > (T.Inner.Dx()-colXPos[i])+1 {
			continue
		}
		buf.SetString(
			h,
			ui.NewStyle(ui.Theme.Default.Fg, ui.ColorClear, ui.ModifierBold),
			image.Pt(T.Inner.Min.X+colXPos[i]-1, T.Inner.Min.Y),
		)
	}

	if T.TopRow < 0 {
		log.Printf("table widget TopRow value less than 0. TopRow: %v", T.TopRow)
		return
	}

	// prints each row
	for rowNum := T.TopRow; rowNum < T.TopRow+T.Inner.Dy()-1 && rowNum < len(T.Rows); rowNum++ {
		style := T.Style
		row := T.Rows[rowNum]
		y := (rowNum + 2) - T.TopRow

		// prints cursor
		if T.ShowCursor {
			if (T.SelectedItem == "" && rowNum == T.SelectedRow) || (T.SelectedItem != "" && T.SelectedItem == row[T.UniqueCol]) {
				style = T.CursorStyle
				for _, width := range T.ColWidths {
					if width == 0 {
						continue
					}
					buf.SetString(
						strings.Repeat(" ", T.Inner.Dx()),
						style,
						image.Pt(T.Inner.Min.X, T.Inner.Min.Y+y-1),
					)
				}
				T.SelectedItem = row[T.UniqueCol]
				T.SelectedRow = rowNum
			}
		}
		// prints each col of the row
		for i, width := range T.ColWidths {
			if width == 0 {
				continue
			}
			// don't render column if width is greater than distance to end of widget
			if width > (T.Inner.Dx()-colXPos[i])+1 {
				continue
			}
			r := ui.TrimString(row[i], width)
			buf.SetString(
				r,
				style,
				image.Pt(T.Inner.Min.X+colXPos[i]-1, T.Inner.Min.Y+y-1),
			)
		}
	}
}

func (T *Table) drawLocation(buf *ui.Buffer) {
	total := len(T.Rows)
	topRow := T.TopRow + 1
	bottomRow := T.TopRow + T.Inner.Dy() - 1
	if bottomRow > total {
		bottomRow = total
	}

	loc := fmt.Sprintf(" %d - %d of %d ", topRow, bottomRow, total)

	width := len(loc)
	buf.SetString(loc, T.TitleStyle, image.Pt(T.Max.X-width-2, T.Min.Y))
}

// Scrolling ///////////////////////////////////////////////////////////////////

// calcPos is used to calculate the cursor position and the current view into the table.
func (T *Table) calcPos() {
	T.SelectedItem = ""

	if T.SelectedRow < 0 {
		T.SelectedRow = 0
	}
	if T.SelectedRow < T.TopRow {
		T.TopRow = T.SelectedRow
	}

	if T.SelectedRow > len(T.Rows)-1 {
		T.SelectedRow = len(T.Rows) - 1
	}
	if T.SelectedRow > T.TopRow+(T.Inner.Dy()-2) {
		T.TopRow = T.SelectedRow - (T.Inner.Dy() - 2)
	}
}

// ScrollUp ...
func (T *Table) ScrollUp() {
	T.SelectedRow--
	T.calcPos()
}

// ScrollDown ...
func (T *Table) ScrollDown() {
	T.SelectedRow++
	T.calcPos()
}

// ScrollTop ...
func (T *Table) ScrollTop() {
	T.SelectedRow = 0
	T.calcPos()
}

// ScrollBottom ...
func (T *Table) ScrollBottom() {
	T.SelectedRow = len(T.Rows) - 1
	T.calcPos()
}

// ScrollHalfPageUp ...
func (T *Table) ScrollHalfPageUp() {
	T.SelectedRow = T.SelectedRow - (T.Inner.Dy()-2)/2
	T.calcPos()
}

// ScrollHalfPageDown ...
func (T *Table) ScrollHalfPageDown() {
	T.SelectedRow = T.SelectedRow + (T.Inner.Dy()-2)/2
	T.calcPos()
}

// ScrollPageUp ...
func (T *Table) ScrollPageUp() {
	T.SelectedRow -= (T.Inner.Dy() - 2)
	T.calcPos()
}

// ScrollPageDown ...
func (T *Table) ScrollPageDown() {
	T.SelectedRow += (T.Inner.Dy() - 2)
	T.calcPos()
}

// HandleClick ...
func (T *Table) HandleClick(x, y int) {
	x = x - T.Min.X
	y = y - T.Min.Y
	if (x > 0 && x <= T.Inner.Dx()) && (y > 0 && y <= T.Inner.Dy()) {
		T.SelectedRow = (T.TopRow + y) - 2
		T.calcPos()
	}
}
