package main

func NewHistoryMode(k Keys) *Mode {
	m := make(Mode)
	m.setKeys(k.ScrollTop, func(t *Table) { t.ScrollTop() })
	m.setKeys(k.ScrollBottom, func(t *Table) { t.ScrollBottom() })
	m.setKeys(k.ScrollUp, func(t *Table) { t.ScrollUp() })
	m.setKeys(k.ScrollDown, func(t *Table) { t.ScrollDown() })
	m.setKeys(k.ScrollPageUp, func(t *Table) { t.ScrollPageUp() })
	m.setKeys(k.ScrollHalfPageUp, func(t *Table) { t.ScrollHalfPageUp() })
	m.setKeys(k.ScrollHalfPageDown, func(t *Table) { t.ScrollHalfPageDown() })
	return &m
}

func (m *Mode) setKeys(keys []string, f func(*Table)) {
	for i := 0; i < len(keys); i++ {
		(*m)[keys[i]] = f
	}
}

// ------------------------- Actions ---------------------------
