package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gopkg.in/yaml.v3"
)

// configurations
var (
	historyFile = "$HOME/.swiperd_history"
	configFiles = [3]string{"$SWIPERD", "$HOME/.config/swiperd/swiperd.yml", "$HOME/.swiperd_config"}
)

// DefaultSettings will store key binding config.
var DefaultSettings = Settings{
	Keys: Keys{
		ScrollTop:          []string{"<Home>", "g, g"},
		ScrollBottom:       []string{"G", "<End>"},
		ScrollUp:           []string{"e", "k", "<Up>", "<MouseWheelUp>"},
		ScrollDown:         []string{"n", "j", "<Down>", "<MouseWheelDown>"},
		ScrollPageUp:       []string{"<C-b>"},
		ScrollPageDown:     []string{"<C-f>"},
		ScrollHalfPageUp:   []string{"<C-u>"},
		ScrollHalfPageDown: []string{"<C-d>"},
	},
	Colorscheme: DefaultColorscheme,
}

type Settings struct {
	Keys        Keys        `yaml:"Keys"`
	Colorscheme Colorscheme `yaml:"Color_Scheme"`
}

type Keys struct {
	ScrollTop          []string `yaml:"top"`
	ScrollBottom       []string `yaml:"bottom"`
	ScrollUp           []string `yaml:"up"`
	ScrollDown         []string `yaml:"down"`
	ScrollPageUp       []string `yaml:"page_up"`
	ScrollPageDown     []string `yaml:"page_down"`
	ScrollHalfPageUp   []string `yaml:"half_page_up"`
	ScrollHalfPageDown []string `yaml:"half_page_down"`
}

// Colorscheme of rendered swiperd TUI
type Colorscheme struct {
	Fg int
	Bg int

	BorderLabel int
	BorderLine  int
	Cursor      int
}

// Default Color scheme
var DefaultColorscheme = Colorscheme{
	Fg:          7,
	Bg:          0,
	BorderLabel: 7,
	BorderLine:  6,
	Cursor:      4,
}


func loadConfig() (Settings, error) {
	config := DefaultSettings
	var rawData []byte
	var err error
	for _, file := range configFiles {
		f := os.ExpandEnv(file)
		rawData, err = os.ReadFile(f)
		if err == nil {
			break
		} else if os.IsNotExist(err) {
			continue
		}
		return config, fmt.Errorf("failed to read file: %s", f)
	}
	err = yaml.Unmarshal(rawData, &config)
	if err != nil {
		return config, fmt.Errorf("cannot unmarshal data: %v", err)
	}
	return config, nil
}

type dataType int

const (
	unknownLine dataType = iota
	historyLine
	favoritesLine
)

func loadHistory() history {
	pathHistory = history{}
	file := os.ExpandEnv(historyFile)
	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	r := bufio.NewScanner(f)

	mode := unknownLine
READLINE:
	for r.Scan() {
		line := r.Text()
		switch strings.TrimSpace(line) {
		case "[[history]]":
			mode = historyLine
			continue READLINE
		case "[[favorites]]":
			mode = favoritesLine
			continue READLINE
		}
		switch mode {
		case historyLine:
			parseHistoryLine(r.Text())
		case favoritesLine:
			parseFavoritesLine(r.Text())
		}

	}

	data, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	for _, fields := range data {
		sec, _ := strconv.Atoi(fields[1])
		t := time.Unix(int64(sec), 0)
		pathHistory = append(history{location{fields[0], t}}, pathHistory...)
	}
	return pathHistory
}

func writeDefaultSettings(file string) error {
	f := os.ExpandEnv(file)
	data, err := yaml.Marshal(&DefaultSettings)
	if err != nil {
		return err
	}
	fmt.Println(string(data))
	parent := filepath.Dir(f)

	if err = os.MkdirAll(parent, 0755); err != nil {
		return err
	}
	if err = os.WriteFile(f, data, 0755); err != nil {
		return err
	}
	return nil
}
