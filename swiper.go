package main

import (
	"fmt"
	"os"
	"sort"
	"strings"
	"time"
)

type swiperWidget struct {
	*Table
	updateInterval time.Duration
	pathHistory    history
}

// NewSwiperWidget ...
func newSwiperWidget() *swiperWidget {
	sw := &swiperWidget{
		Table:          NewTable(),
		updateInterval: time.Second,
	}
	h := loadHistory()
	h.RemoveDumplicate()
	h.SortByTime()
	sw.pathHistory = h

	sw.Title = " Swiper "
	sw.ShowCursor = true
	sw.ShowLocation = true
	sw.ColGap = 3
	sw.PadLeft = 2
	sw.ColResizer = func() { sw.ColWidths = []int{3, maxInt(sw.Inner.Dx()-23, h.LenLongestPath()-23), 10} }
	sw.SelectedRow = 1
	sw.UniqueCol = 0

	sw.update()
	go func() {
		for range time.NewTicker(sw.updateInterval).C {
			sw.Lock()
			sw.update()
			sw.Unlock()
		}
	}()
	return sw
}

func (l location) Path() string {
	p := l.path
	home := os.Getenv("HOME")
	return strings.Replace(p, home, "~", 1)
}

func (l location) Time() string {
	var t string
	today := time.Now().Truncate(24 * time.Hour)
	if l.time.Before(today) {
		t = l.time.Local().Format("Mon Jan _2")
	} else {
		t = l.time.Local().Format(time.Kitchen)
	}
	return t

}

func (sw *swiperWidget) update() {
	sw.pathHistory.RemoveInvalid()
	sw.updateTable()
}

func (sw *swiperWidget) updateTable() {
	table := make([][]string, len(sw.pathHistory))

	for i, p := range sw.pathHistory {
		table[i] = make([]string, 3)
		table[i][0] = fmt.Sprint(i)
		table[i][1] = p.Path()
		table[i][2] = p.Time()
	}
	sw.Rows = table
}

func maxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func (ps *pathSorter) Len() int {
	return len(ps.paths)
}

func (ps *pathSorter) Swap(i, j int) {
	ps.paths[i], ps.paths[j] = ps.paths[j], ps.paths[i]
}

// Less ...
func (ps *pathSorter) Less(i, j int) bool {
	return ps.by(&ps.paths[i], &ps.paths[j])
}

// By is the sort function contract
type By func(p1, p2 *location) bool

type pathSorter struct {
	paths history
	by    func(p1, p2 *location) bool
}

// Sort ...
func (by By) Sort(h history) {
	ps := &pathSorter{
		paths: h,
		by:    by,
	}
	sort.Sort(ps)
}
func (h history) SortByPath() {
	path := func(p1, p2 *location) bool {
		return p1.path < p2.path
	}
	By(path).Sort(h)
}

func (h history) SortByTime() {
	time := func(p1, p2 *location) bool {
		return p1.time.After(p2.time)
	}
	By(time).Sort(h)
}

func (h *history) RemoveDumplicate() {
	h.SortByPath()
	length := len(*h)
	for i := 1; i < length; i++ {
		for (*h)[i].path == (*h)[i-1].path {

			if (*h)[i].time.Before((*h)[i-1].time) {
				h.Remove(i)
			} else {
				h.Remove(i - 1)
			}
			length--
			if i >= length-1 {
				break
			}
		}
	}
}

func (h *history) RemoveInvalid() {
	length := len(*h)
	for i := 1; i < length; i++ {
		for !exists((*h)[i].path) {
			h.Remove(i)
			length--
			i--
		}
	}
}

// exists returns whether the given file or directory exists
func exists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func (h *history) Remove(i int) {
	*h = append((*h)[:i], (*h)[i+1:]...)
}

func (h history) LenLongestPath() int {
	max := 0
	for l := range h {
		if m := len(h[l].Path()); m > max {
			max = m
		}
	}
	return max
}
