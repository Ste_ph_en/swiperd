package main

type Config struct {
	Colorscheme Colorscheme `yaml:",inline"`
}

type Colorscheme struct {
	Name   string
	Author string
	Fg     int `yaml:"fg" default:"7"`
	Bg     int `yaml:"bg" default:"-1"`

	BorderLabel int `yaml:"bg"`
	BorderLine  int `yaml:"bg"`

	// should add at least 8 here
	CPULines []int `yaml:"bg"`

	BattLines []int `yaml:"bg"`

	MainMem int `yaml:"bg"`
	SwapMem int

	ProcCursor int `yaml:"bg"`

	Sparkline int `yaml:"bg"`

	DiskBar int `yaml:"bg"`
}
